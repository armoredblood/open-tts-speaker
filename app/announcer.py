"""
Local and Offline Capable Home Automation
"""
import os
import hashlib
from pydub import AudioSegment
from pydub.playback import play
from google.cloud import texttospeech
from flask import Flask
import multiprocessing
import queue

from .decorators import singleton
from app import app

@singleton
class Announcer:
    def __init__(self):
        
        # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/aaron/Documents/automation_announcer/armoredblood-home-fe336a3fee40.json"
        os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = app.config['GCP_CREDENTIALS']
        self.gcp_tts_voice = app.config['GCP_TTS_VOICE']

        # setup multiprocessing pool and queue
        multiprocessing.set_start_method = "fork"
        self.speechQueue = multiprocessing.Queue()
        self.consumerProcess = multiprocessing.Process(target=self.speak, args=(self.speechQueue,))
        self.consumerProcess.start()  

    def playSound(self, filename): 
        sound = AudioSegment.from_file(filename, format="wav")
        play(sound)

    def generateWav(self, text, filename, voice):

        # Instantiates a client
        client = texttospeech.TextToSpeechClient()

        # Set the text input to be synthesized
        synthesis_input = texttospeech.SynthesisInput(text=text)

        # Build the voice request, select the language code ("en-US") and the ssml
        # voice gender ("neutral")
        voice = texttospeech.VoiceSelectionParams(
            name=voice, language_code=voice[:5]
        )

        # Select the type of audio file you want returned
        audio_config = texttospeech.AudioConfig(
            audio_encoding=texttospeech.AudioEncoding.LINEAR16
        )

        # Perform the text-to-speech request on the text input with the selected
        # voice parameters and audio file type
        response = client.synthesize_speech(
            input=synthesis_input, voice=voice, audio_config=audio_config
        )

        # The response's audio_content is binary.
        with open(f"{filename}", "wb") as out:
            out.write(response.audio_content)
            print(f'Audio content written to file "{filename}"')

        # combine new wav file with brown noise to wake up usb audio device
        sound1 = AudioSegment.from_wav("wav/brown_noise.wav")
        sound2 = AudioSegment.from_wav(filename)

        adjustedsound2 = sound2.apply_gain_stereo(-10, -10)

        combined_sounds = sound1 + adjustedsound2
        combined_sounds.export(f'{filename}', format="wav")

    def speak(self, speechQueue):

        try:

            # exaust the current queue
            while True:

                # text = speechQueue.get(timeout=CONSUMER_TIMEOUT)
                text = self.speechQueue.get()

                toHash = f"{self.gcp_tts_voice}:{text}"
                filename = f"wav/{hashlib.sha1(bytes(toHash.encode())).hexdigest()}.wav"

                try:

                    print(f"announcing \"{text}\" via file {filename}")
                    self.playSound(filename)

                except:

                    print("file not found. generating new file")
                    self.generateWav(text, filename, self.gcp_tts_voice)

                    print(f"final attempt to play file {filename}")
                    self.playSound(filename)


        except queue.Empty:

            print("Consumer:timeout...nothing to read")

    def enqueueSpeech(self, text):
        self.speechQueue.put(text)