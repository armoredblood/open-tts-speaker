from flask import request

from app import app
from .announcer import Announcer
from .decorators import require_appkey

@app.route('/speak')
@require_appkey
def announce():
    text = request.args.get('phrase')
    print(f"sending {text} to enqueueSpeech")
    announcer = Announcer.Instance()
    announcer.enqueueSpeech(text)
    return "ok"