# ensures only one instance of the decorated class exists in memory
class singleton:

    def __init__(self, cls):
        self._cls = cls

    def Instance(self):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._cls()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._cls)


# validate apikey
from functools import wraps
from flask import request, abort
import hashlib
from app import app

def require_appkey(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):

        key = request.args.get('key')

        if key is not None:

            phrase = request.args.get('phrase')
            password = app.config['PASSWORD']
            toHash = f"{password}:{phrase}"

            apiKey = hashlib.sha256(bytes(toHash.encode())).hexdigest()

            if apiKey == key:
                return view_function(*args, **kwargs)
            else:
                abort(401)
        else:
            abort(401)
    return decorated_function