# open-tts-speaker

## How to Run
install dependencies
```
sudo apt-get install -y python3-dev libasound2-dev ffmpeg
```

install pipenv
```
python3 -m pip install pipenv
```

install project libraries
```
pipenv install
```

run flask
```
export PIPENV_VENV_IN_PROJECT=1
export FLASK_RUN run.py
flask run
```

## GCP TTS voices I like
* en-us-wavenet-h speed:0.90
* en-us-wavenet-d speed:1.0
* en-us-wavenet-f speed:1.0
* en-us-wavenet-g speed:1.0 ***
* en-us-wavenet-i speed:1.0
* en-gb-wavenet-a speed:1.0
* en-gb-wavenet-c speed:1.0 ***