import hashlib
import sys
import requests

list_of_arguments = sys.argv

if list_of_arguments[1]:
    phrase = list_of_arguments[1]
    print(phrase)
    password = "b34DAXby9zm4!"
    toHash = f"{password}:{phrase}"
    apiKey = hashlib.sha256(bytes(toHash.encode())).hexdigest()

    x = requests.get(
        url=f"http://192.168.30.83:5000/speak",
        params={ 
            'phrase' : phrase, 
            'key' : apiKey
        }
    )
    print(x.status_code)

else:
    print("please provide something to announce")
